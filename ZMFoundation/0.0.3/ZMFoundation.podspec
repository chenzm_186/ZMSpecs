# 本地测试
Pod::Spec.new do |s|
  # 项目名
  s.name             = 'ZMFoundation'
  # 版本号
  s.version          = '0.0.3'
  # 简述
  s.summary          = '个人组件库'
  # 详述
  s.description      = <<-DESC
    TODO: 在这里添加pod的详细描述。
  DESC

  # 项目主页
  s.homepage         = 'https://www.baidu.com/'
  # 项目需要遵守的协议
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  # 作者邮箱
  s.author           = { 'chenzimin' => '1005332621@qq.com' }
  # 项目最低支持版本
  s.ios.deployment_target = '8.0'

  # ===================  所有子项目  =======================
  s.subspec 'WCDB' do |ss|
    # 1.0.6
    ss.vendored_frameworks = 'Products/WCDB/*.framework'
    ss.libraries = "z", "c++"
    ss.frameworks = "CoreFoundation", "Security", "Foundation"
  end

  s.subspec 'ZipArchive' do |ss|
    # 1.4.0
    ss.source_files = 'Products/ZipArchive/*.{h,hpp}'
    ss.public_header_files = 'Products/ZipArchive/*.{h,hpp}'
    ss.vendored_library = 'Products/ZipArchive/*.a'
  end
  # ===================  所有子项目  =======================


  # git仓库地址
  s.source           = { :git => 'https://gitee.com/chenzm_186/ZMFoundation.git', :tag => s.version.to_s }
  # 设置默认下载实例,没有设置则下载所有
  # All
  s.default_subspec='All'
  s.subspec 'All' do |ss|
    ss.dependency 'ZMFoundation/WCDB'
    ss.dependency 'ZMFoundation/ZipArchive'
  end

end
# ************************************************************
